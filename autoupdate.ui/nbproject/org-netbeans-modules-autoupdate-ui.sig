#Signature file v4.1
#Version 1.27.3

CLSS public java.lang.Object
cons public init()
meth protected java.lang.Object clone() throws java.lang.CloneNotSupportedException
meth protected void finalize() throws java.lang.Throwable
meth public boolean equals(java.lang.Object)
meth public final java.lang.Class<?> getClass()
meth public final void notify()
meth public final void notifyAll()
meth public final void wait() throws java.lang.InterruptedException
meth public final void wait(long) throws java.lang.InterruptedException
meth public final void wait(long,int) throws java.lang.InterruptedException
meth public int hashCode()
meth public java.lang.String toString()

CLSS public final org.netbeans.modules.autoupdate.ui.api.PluginManager
meth public static boolean openInstallWizard(org.netbeans.api.autoupdate.OperationContainer<org.netbeans.api.autoupdate.InstallSupport>)
supr java.lang.Object

