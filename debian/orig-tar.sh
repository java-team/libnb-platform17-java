#!/bin/bash -e

VERSION=$2
FILE=$3

FILENAME=`basename $FILE`
BASE=`basename $FILE .zip`
DIR=`dirname $FILE`

if [ "x$BASE.zip" != "x$FILENAME" ]
then
# The file isn't the real file, but we can use it to find the real
# file and download it.
    SITE="http://dlc.sun.com.edgesuite.net/netbeans/$VERSION/final/zip/"
    DL_FILE=`perl -ne 'if(/HREF="(netbeans-[0-9.]+-\d+-platform-src.zip)"/) {print $1."\n" }' $FILE | tail -n 1`
    rm -f $FILE
    FILE="$DIR/$DL_FILE"
    
    if [ ! -e $FILE ]
    then
	echo "Downloading real file $DL_FILE from $SITE"
	wget -O "$FILE" "$SITE/$DL_FILE"
    fi

fi

TARFILE=../libnb-platform17-java_$VERSION+dfsg1.orig.tar.gz
# Resolve symbolic links
FILE=`readlink -f $FILE`
BASE="libnb-platform-java-$VERSION"

echo "Extracting zip file..."
mkdir $BASE
unzip -q -d $BASE $FILE

echo "Cleaning source code..."

# Remove jar files - we'll use debian packaged jars from
# /usr/share/java
find $BASE/ -name "*.jar" -type f -execdir rm \{\} \;

# Remove windows executables and libraries - we'll try and build those
# which we need.
find $BASE/ \( -name "*.exe" -o -name "*.dll" -o -name "*.res" \) -execdir rm \{\} \;

# Remove Visual Studio files
find $BASE/ \( -name "*.sln" -o -name "*.suo" -o -name "*.ncb" -o -name "*.idb" \) -execdir rm \{\} \;

# Remove Mac specific files. We can't regenerate these.
find $BASE/ \( -name "*.dylib" -o -name "*.dmg" -o -name "objects.nib" \) -execdir rm \{\} \;

# Remove shared library (.so) files. Hopefully we don't need them
find $BASE/ -name "*.so" -type f -execdir rm \{\} \;

# remove unzipsfx binaries
find $BASE/ -name "unzipsfx" -type f -execdir rm \{\} \;

# Remove files containing serialized java objects (maybe we can
# re-create these if we need)
find $BASE/ -name "*.ser" -type f -execdir rm \{\} \;

# Remove class files
find $BASE/ -name "*.class" -type f -execdir rm \{\} \;

# Remove some files where it's not clear how they were generated, and
# which we (hopefully) don't need
find $BASE/ -name "url.url" -type f -execdir rm \{\} \;
find $BASE/ -name "unrecognized" -type f -execdir rm \{\} \;

# Remove all the .nbm files. They are only used in unit testing, and
# we'd ideally need to generate them as part of the build.
find $BASE/ -name "*.nbm" -type f -execdir rm \{\} \;

# Remove object files
find $BASE/ -name "*.o" -type f -execdir rm \{\} \;

# Remove .data files
find $BASE/ -name "*.data" -type f -execdir rm \{\} \;

# Remove data files
find $BASE/ -name "data" -type f -execdir rm \{\} \;

# Remove .bin files
find $BASE/ -name "*.bin" -type f -execdir rm \{\} \;

# Remove .cfs files
find $BASE/ -name "*.cfs" -type f -execdir rm \{\} \;

# Remove .gen files
find $BASE/ -name "*.gen" -type f -execdir rm \{\} \;

# Remove .gen files
find $BASE/ -name "segments_2" -type f -execdir rm \{\} \;

# Remove some ELF binaries
rm -rf $BASE/dlight.tools/release/tools/*

find $BASE/ -name "*.sl" -type f -execdir rm \{\} \;
find $BASE/ -name "*.jnilib" -type f -execdir rm \{\} \;

# Remove framemaker files and the pdf they build, since we can't edit them
# with software in Debian.
rm -f $BASE/usersguide/tutorials/j2ee-tut/fm/*

# remove .gz files. We could re-create at least some if we need, but they're only
# used in unit tests.
find $BASE/ -name "*.gz" -type f -execdir rm \{\} \;

# Remove .warContent files since they are binary files we can't build.
find $BASE/ -name "*.warContent" -type f -execdir rm \{\} \;

# Remove .jts media files as we don't appear to have a codec for them
find $BASE/ -name "*.jts" -type f -execdir rm \{\} \;

# Remove .pdf media files. We can recreate the ones in ide.branding
find $BASE/ -name "*.pdf" -type f -execdir rm \{\} \;

# Remove binary .out files.
find $BASE/ -name "*.out" -type f -execdir rm \{\} \;

# Remove zip files. We'll have to reproduce some.
find $BASE/ -name "*.zip" -type f -execdir rm \{\} \;
find $BASE/ -name "*.cap" -type f -execdir rm \{\} \;
find $BASE/ -name "*.eap" -type f -execdir rm \{\} \;
find $BASE/ -name "*.war" -type f -execdir rm \{\} \;
find $BASE/ -name "*.ear" -type f -execdir rm \{\} \;


# Remove bits of a version control repository
rm -rf $BASE/versioning.util/test/unit/data/


# Make our ".orig.tar.gz"
tar -czf $TARFILE $BASE

# Remove our unpack directory
rm -rf $BASE

echo "Done."
